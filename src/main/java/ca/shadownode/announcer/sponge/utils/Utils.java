package ca.shadownode.announcer.sponge.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.serializer.TextSerializers;

public class Utils {
    
    private static final Pattern URL_PATTERN = Pattern.compile("((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)", Pattern.CASE_INSENSITIVE);
    
    public static Text parseMessage(String message, String... args) {
        for (int i = 0; i < args.length; i++) {
            String param = args[i];
            message = message.replace("{" + i + "}", param);
        }

        Text textMessage = TextSerializers.FORMATTING_CODE.deserialize(message);
        List<String> urls = extractUrls(message);
        
        if (urls.isEmpty()) {
            return textMessage;
        }

        Iterator<String> iterator = urls.iterator();
        while (iterator.hasNext()) {
            String url = iterator.next();
            String msgBefore = StringUtils.substringBefore(message, url);
            String msgAfter = StringUtils.substringAfter(message, url);
            if (msgBefore == null) {
                msgBefore = "";
            } else if (msgAfter == null) {
                msgAfter = "";
            }
            try {
                textMessage = Text.of(
                        TextSerializers.FORMATTING_CODE.deserialize(msgBefore),
                        TextActions.openUrl(new URL(url)),
                        Text.of(TextColors.GREEN, url),
                        TextSerializers.FORMATTING_CODE.deserialize(msgAfter));
            } catch (MalformedURLException e) {
                return Text.of(message);
            }

            iterator.remove();
        }
        return textMessage;
    }
   
    public static List<String> extractUrls(String text) {
        List<String> containedUrls = new ArrayList<>();
        Matcher urlMatcher;
        try {
            urlMatcher = URL_PATTERN.matcher(text);
        } catch (Throwable t) {
            return containedUrls;
        }
        while (urlMatcher.find()) {
            containedUrls.add(text.substring(urlMatcher.start(0),
                    urlMatcher.end(0)));
        }

        return containedUrls;
    }

    
}
